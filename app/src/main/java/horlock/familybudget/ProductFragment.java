package horlock.familybudget;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import horlock.familybudget.models.Product;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ProductFragment extends DialogFragment {

    Spinner spinner;
    Realm realm;
    Button doneButton;
    ArrayList<Product> products;
    TextView price, fullPrice;
    public int currentPosition = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.product_fragment, container, false);
        spinner = (Spinner) rootView.findViewById(R.id.product_month);
        price = (TextView)rootView.findViewById(R.id.product_price);
        fullPrice = (TextView)rootView.findViewById(R.id.product_year);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.month, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(adapter);

        final RealmConfiguration realmConfig = new RealmConfiguration.Builder(getContext()).build();
        realm = Realm.getInstance(realmConfig);
        RealmResults<Product> realmResults = realm.where(Product.class).findAll();
        products = new ArrayList<>();
        copyToProductList(realmResults, products);
        updateTotalPrice();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView currentMonth = (TextView) view;
                boolean checker = false;
                if (products != null && !products.isEmpty()) {
                    for (int i = 0; i < products.size(); i++) {
                        if (products.get(i).getMonth().equals(currentMonth.getText())) {
                            checker = true;
                            currentPosition = i;
                            if (products.get(i).getMoney() != 0) {
                                price.setText(String.valueOf(products.get(i).getMoney()));
                            } else {
                                price.setText("");
                            }

                        }
                    }
                }
                if (!checker) {
                    price.setText("");
                    Product newProduct = new Product();
                    newProduct.setMonth(String.valueOf(currentMonth.getText()));
                    products.add(newProduct);
                    currentPosition = products.size() - 1;
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                price.setText("");
            }
        });

        doneButton = (Button) rootView.findViewById(R.id.product_done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                realm.clear(Product.class);
                for (int i = 0; i < products.size(); i++) {
                    realm.copyToRealmOrUpdate(products.get(i));
                }
                realm.commitTransaction();
                dismiss();
            }
        });

        price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!TextUtils.isEmpty(s)) {
                    products.get(currentPosition).setMoney(Integer.parseInt(s.toString()));
                    updateTotalPrice();
                }
            }
        });

        return rootView;
    }

    private void copyToProductList(RealmResults<Product> realmResults, ArrayList<Product> products) {
        if(realmResults != null && !realmResults.isEmpty()){
            for (int i = 0; i < realmResults.size(); i++) {
                Product product = new Product();
                product.setMonth(realmResults.get(i).getMonth());
                product.setMoney(realmResults.get(i).getMoney());
                products.add(i, product);
            }

        }
    }

    public void updateTotalPrice() {
        int totalPrice = 0;
        if (products != null && !products.isEmpty()) {
            for (int i = 0; i < products.size(); i++) {
                totalPrice += products.get(i).getMoney();
            }
            fullPrice.setText(String.valueOf(totalPrice));
        }
    }
}
