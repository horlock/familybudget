package horlock.familybudget;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import horlock.familybudget.models.Tax;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class TaxActivity extends AppCompatActivity {

    private TaxRecyclerViewAdapter taxRecyclerViewAdapter;
    private Realm realm;
    ImageButton addButton, doneButton;
    TextView fullPrice;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tax_activity);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        // Create a RealmConfiguration which is to locate Realm file in package's "files" directory.
        final RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).build();
        // Get a Realm instance for this thread
        realm = Realm.getInstance(realmConfig);
        RealmResults<Tax> realmResults = realm.where(Tax.class).findAll();
        ArrayList<Tax> taxArrayList = new ArrayList<>();
        copyDataFromRealm(realmResults, taxArrayList);
        taxRecyclerViewAdapter = new TaxRecyclerViewAdapter(taxArrayList);
        recyclerView.setAdapter(taxRecyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        addButton = (ImageButton)findViewById(R.id.add_button);
        doneButton = (ImageButton)findViewById(R.id.tax_done);
        fullPrice = (TextView)findViewById(R.id.full_price);
        checkForNull(taxArrayList);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tax tax = new Tax();
                taxRecyclerViewAdapter.addTax(tax);
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                realm.clear(Tax.class);
                for(int i = 0; i < taxRecyclerViewAdapter.getTaxSize(); i++) {
                    if(taxRecyclerViewAdapter.getTax(i).getName() != null)
                    realm.copyToRealmOrUpdate(taxRecyclerViewAdapter.getTax(i));
                }
                realm.commitTransaction();
                finish();
            }
        });
    }

    private void checkForNull(ArrayList<Tax> taxArrayList) {
        if(taxArrayList != null && !taxArrayList.isEmpty())
            updateTotalPrice();
    }

    private void copyDataFromRealm(RealmResults<Tax> realmResults, ArrayList<Tax> taxArrayList) {
        for(int i = 0; i < realmResults.size(); i++){
            Tax tax = new Tax();
            tax.setName(realmResults.get(i).getName());
            tax.setA(realmResults.get(i).getA());
            tax.setB(realmResults.get(i).getB());
            tax.setC(realmResults.get(i).getC());
            taxArrayList.add(i, tax);
        }
    }

    public void updateTotalPrice() {
        int totalPrice = 0;
        for(int i = 0; i < taxRecyclerViewAdapter.getTaxSize(); i++){
            totalPrice += taxRecyclerViewAdapter.getTax(i).getC();
        }
        fullPrice.setText(String.valueOf(totalPrice));
    }
}