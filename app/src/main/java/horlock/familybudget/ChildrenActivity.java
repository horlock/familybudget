package horlock.familybudget;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;

import horlock.familybudget.models.Children;
import io.realm.Realm;
import io.realm.RealmResults;

public class ChildrenActivity extends AppCompatActivity {

    public Spinner spinner;
    public ArrayList<Children> childrenArrayList;
    public RecyclerView recyclerView;
    public ChildrenAdapter childrenAdapter;
    public ImageButton addButton, saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.children_activity);

        spinner = (Spinner) findViewById(R.id.energy_month);
        addButton = (ImageButton) findViewById(R.id.add_button);
        saveButton = (ImageButton) findViewById(R.id.children_done);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.month, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(adapter);
        RealmResults<Children> realmResults = Realm.getInstance(this)
                .where(Children.class)
                .equalTo("month", Calendar.MONTH)
                .findAll();
        childrenArrayList = new ArrayList<>();
        copyToClothesList(realmResults, childrenArrayList);

        recyclerView = (RecyclerView) findViewById(R.id.children_recycler_view);
        childrenAdapter = new ChildrenAdapter();
        childrenAdapter.setList(childrenArrayList);
        recyclerView.setAdapter(childrenAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                RealmResults<Children> realmResults = Realm.getInstance(getBaseContext())
                        .where(Children.class)
                        .equalTo("month", position)
                        .findAll();
                copyToClothesList(realmResults, childrenArrayList);
                childrenAdapter.setList(childrenArrayList);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Children child = new Children();
                childrenAdapter.addChild(child);
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void copyToClothesList(RealmResults<Children> realmResults, ArrayList<Children> childrenArrayList) {
        if(realmResults != null && !realmResults.isEmpty()){
            for(int i = 0; i < realmResults.size(); i++){
                Children children = new Children();
                children.setMoney(realmResults.get(i).getMoney());
                children.setMonth(realmResults.get(i).getMonth());
                children.setName(realmResults.get(i).getName());
                childrenArrayList.add(i, children);
            }
        }
    }

    public void fullCopy(ArrayList<Children> childrenArrayList, ArrayList<Children> total){
        if(childrenArrayList != null && !childrenArrayList.isEmpty()){
            for(int i = 0; i < childrenArrayList.size(); i++){
                RealmResults<Children> realmResults = Realm.getInstance(ChildrenActivity.this)
                        .where(Children.class)
                        .equalTo("name", childrenArrayList.get(i).getName())
                        .findAll();
            }
        }
    }
}
