package horlock.familybudget;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import horlock.familybudget.models.Tax;

public class TaxRecyclerViewAdapter extends RecyclerView.Adapter<TaxRecyclerViewAdapter.RecyclerViewHolder> {

    ArrayList<Tax> addChanges;


    public TaxRecyclerViewAdapter(ArrayList<Tax> realmResults) {
        super();

        addChanges = realmResults;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.tax_layout, parent, false);
        return new RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.setTax(addChanges.get(position));
    }

    @Override
    public int getItemCount() {
        if(addChanges != null){
            return addChanges.size();
        }else {
            return 0;
        }
    }

    public int getTaxSize(){
        return addChanges.size();
    }

    public Tax getTax(int position){
        return addChanges.get(position);
    }

    public void addTax(Tax tax) {
        addChanges.add(tax);
        notifyItemInserted(addChanges.size());
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public TextView name, a, b, c;
        private Tax localTax;
        private int first, second, third;

        private Runnable requestUpdateTotalPrice = new Runnable() {
            @Override
            public void run() {
                TaxActivity context = (TaxActivity) itemView.getContext();
                context.updateTotalPrice();
            }
        };

        public RecyclerViewHolder(View v) {
            super(v);

            name = (TextView) v.findViewById(R.id.tax_name);
            a = (TextView) v.findViewById(R.id.tax_a);
            b = (TextView) v.findViewById(R.id.tax_b);
            c = (TextView) v.findViewById(R.id.tax_c);


        }

        public void setTax(Tax tax) {
            localTax = tax;

            name.setText("");
            a.setText("");
            b.setText("");
            c.setText("");

            if(tax.getName() != null){
                name.setText(tax.getName());
            }
            if(tax.getA() != 0){
                a.setText(String.valueOf(tax.getA()));
            }
            if(tax.getB() != 0){
                b.setText(String.valueOf(tax.getB()));
            }
            if(tax.getC() != 0){
                c.setText(String.valueOf(tax.getC()));
            }

            name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!TextUtils.isEmpty(s)){
                        localTax.setName(s.toString());
                    }
                }
            });
            a.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!TextUtils.isEmpty(s)) {
                        localTax.setA(Integer.parseInt(s.toString()));
                        first = Integer.parseInt(s.toString());
                        countC();
                    }
                }
            });
            b.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!TextUtils.isEmpty(s)){
                        localTax.setB(Integer.parseInt(s.toString()));
                        second = Integer.parseInt(s.toString());
                        countC();
                    }
                }
            });
            c.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    itemView.removeCallbacks(requestUpdateTotalPrice);
                    itemView.postDelayed(requestUpdateTotalPrice, 100);
                }
            });
        }
        public void countC(){
            third = first * second;
            c.setText(String.valueOf(third));
            localTax.setC(third);
        }
    }
}
