package horlock.familybudget.models;

import android.content.Context;
import io.realm.RealmObject;

public class Transaction extends RealmObject {
  private int month;
  private String type;
  private String name;
  private double transferred;

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getTransferred() {
    return transferred;
  }

  public void setTransferred(double transferred) {
    this.transferred = transferred;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public static class TransactionComposition implements ITransactionComposition<Transaction> {
    @Override public double getTransferred(Transaction transaction) {
      return transaction.getTransferred();
    }

    @Override public void setTransferred(Transaction transaction, double transferred) {
      transaction.setTransferred(transferred);
    }

    @Override public boolean isIncome(Transaction transaction) {
      return "income".equals(transaction.getName());
    }

    @Override public void setName(Transaction transaction, CharSequence name) {
      transaction.setName(name.toString());
    }

    @Override public CharSequence getName(Transaction transaction) {
      return transaction.getName();
    }

    @Override public CharSequence getCategory(Transaction transaction, Context context) {
      return context.getString(context.getResources()
          .getIdentifier(transaction + "_title", "string", context.getPackageName()));
    }
  }
}
