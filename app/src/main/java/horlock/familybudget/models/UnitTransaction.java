package horlock.familybudget.models;

import android.content.Context;
import android.content.SharedPreferences;
import io.realm.RealmObject;

public class UnitTransaction extends RealmObject{
  private int month;
  private int startValue;
  private int endValue;
  private double transferred;
  private String type;

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public int getDelta(){
    return endValue - startValue;
  }

  public int getStartValue() {
    return startValue;
  }

  public void setStartValue(int startValue) {
    this.startValue = startValue;
  }

  public int getEndValue() {
    return endValue;
  }

  public void setEndValue(int endValue) {
    this.endValue = endValue;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public double getTransferred() {
    return transferred;
  }

  public void setTransferred(double transferred) {
    this.transferred = transferred;
  }

  @Override public String toString() {
    return "Electricity{" +
        "month=" + month +
        ", startValue=" + startValue +
        ", endValue=" + endValue +
        ", transferred=" + transferred +
        '}';
  }

  public static class UnitTransactionComposition implements ITransactionComposition<UnitTransaction> {
    public static final String GLOBAL_PRICE_PREFERENCE = "global_prices";
    public static final String KEY_ELECTRICITY_PRICE = "price:electricity";
    public static final String KEY_GAZOLINE_PRICE = "price:gazoline";

    private SharedPreferences preferences;

    public UnitTransactionComposition(Context context) {
      this.preferences = context.getSharedPreferences(GLOBAL_PRICE_PREFERENCE,  Context.MODE_PRIVATE);
    }

    public SharedPreferences getPreferences() {
      return preferences;
    }

    @Override public double getTransferred(UnitTransaction transaction) {
      String key = "".equals(transaction.getType()) ? KEY_ELECTRICITY_PRICE : KEY_GAZOLINE_PRICE;
      return transaction.getTransferred() * preferences.getFloat(key, 0);
    }

    @Override public void setTransferred(UnitTransaction transaction, double transfered) {
      transaction.setTransferred(transfered);
    }

    @Override public boolean isIncome(UnitTransaction transaction) {
      return false;
    }

    @Override public void setName(UnitTransaction transaction, CharSequence name) {

    }

    @Override public CharSequence getName(UnitTransaction transaction) {
      return null;
    }

    @Override public CharSequence getCategory(UnitTransaction transaction, Context context) {
      return context.getString(context.getResources()
          .getIdentifier(transaction + "_title", "string", context.getPackageName()));
    }
  }
}
