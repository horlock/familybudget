package horlock.familybudget.models;

import android.content.Context;
import io.realm.RealmObject;

public interface ITransactionComposition<T extends RealmObject> {

  /**
   * Сколько денег было переведно
   */
  double getTransferred(T transaction);
  void setTransferred(T transaction, double transfered);

  /**
   * Флаг что бы различать пришли или ушли деньги на другой счет
   */
  boolean isIncome(T transaction);

  /**
   * Название транзакции
   */
  void setName(T transaction, CharSequence name);
  CharSequence getName(T transaction);

  /**
   * Возвращаем имя категории транзакции
   */
  CharSequence getCategory(T transaction, Context context);

}
