package horlock.familybudget.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Credit extends RealmObject {
    @PrimaryKey
    private String name;
    private int monthPayment = 0, yearPayment = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMonthPayment() {
        return monthPayment;
    }

    public void setMonthPayment(int monthPayment) {
        this.monthPayment = monthPayment;
    }

    public int getYearPayment() {
        return yearPayment;
    }

    public void setYearPayment(int yearPayment) {
        this.yearPayment = yearPayment;
    }
}
