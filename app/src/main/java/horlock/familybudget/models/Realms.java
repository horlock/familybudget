package horlock.familybudget.models;

import horlock.familybudget.models.UnitTransaction.UnitTransactionComposition;
import horlock.familybudget.v2.TrackBudgetApplication;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Realms {

  public static final HashMap<Class<? extends RealmObject>, ITransactionComposition> REALMS_MAP =
      new HashMap<>();

  public static final List<TransactionsChain> CHAINS =
      Arrays.asList(new TransactionsOutcomeChain(), new UnitTransactionsOutcomeChain(),
          new IncomeTransactionsChain());

  static {
    REALMS_MAP.put(Transaction.class, new Transaction.TransactionComposition());
    REALMS_MAP.put(UnitTransaction.class,
        new UnitTransactionComposition(TrackBudgetApplication.getContext()));
  }

  public interface TransactionsChain {
    void proceed(Realm realm, MoneySpendingInfo.Builder builder);
  }

  public static class TransactionsOutcomeChain implements TransactionsChain {
    @Override public void proceed(Realm realm, MoneySpendingInfo.Builder builder) {
      RealmQuery<Transaction> query =
          realm.where(Transaction.class).notEqualTo("type", "income_journal");
      if (builder.month >= 0 && builder.month <= 11) {
        query.equalTo("month", builder.month());
      }
      builder.plusOutcome(query.sum("transferred").doubleValue());
    }
  }

  public static class UnitTransactionsOutcomeChain implements TransactionsChain {
    private static final String[] UNITS = { "gaz", "electricity" };

    @Override public void proceed(Realm realm, MoneySpendingInfo.Builder builder) {
      UnitTransaction transaction = new UnitTransaction();
      for (String unit : UNITS) {
        RealmQuery<UnitTransaction> query =
            realm.where(UnitTransaction.class).equalTo("type", unit);
        if (builder.month >= 0 && builder.month <= 11) {
          query.equalTo("month", builder.month());
        }
        RealmResults<UnitTransaction> transactions = query.findAll();
        transaction.setTransferred(transactions.sum("transferred").doubleValue());
        //noinspection unchecked
        builder.plusOutcome(REALMS_MAP.get(UnitTransaction.class).getTransferred(transaction));
      }
    }
  }

  public static class IncomeTransactionsChain implements TransactionsChain {
    @Override public void proceed(Realm realm, MoneySpendingInfo.Builder builder) {
      builder.plusIncome(realm.where(Transaction.class)
          .equalTo("type", "income_journal")
          .findAll()
          .sum("transferred")
          .doubleValue());
    }
  }

  public static class MoneySpendingInfo {
    private double income;
    private double outcome;
    private int month;

    private MoneySpendingInfo(double income, double outcome, int month) {
      this.income = income;
      this.outcome = outcome;
      this.month = month;
    }

    public double income() {
      return income;
    }

    public double outcome() {
      return outcome;
    }

    public int month() {
      return month;
    }

    public static class Builder {
      private double income;
      private double outcome;
      private int month;

      public Builder(int month) {
        this.month = month;
      }

      public int month() {
        return month;
      }

      public void plusIncome(double amount) {
        this.income += amount;
      }

      public void plusOutcome(double amount) {
        this.outcome += amount;
      }

      public MoneySpendingInfo build() {
        return new MoneySpendingInfo(income, outcome, month);
      }
    }
  }
}
