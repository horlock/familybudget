package horlock.familybudget;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import horlock.familybudget.v2.ITransactionFragment;
import horlock.familybudget.v2.TotalTransactionsFragment;
import horlock.familybudget.v2.TrackTransactionFragment;
import horlock.familybudget.v2.UnitTrackingFragment;

public class MainActivity extends AppCompatActivity {

  private View.OnClickListener onItemClickListener = new View.OnClickListener() {
    @Override public void onClick(View v) {

      String viewName = v.getResources().getResourceEntryName(v.getId());

      if (v.getId() == R.id.budget) {
        getSupportFragmentManager().beginTransaction()
            .add(android.R.id.content, new TotalTransactionsFragment(),
                TotalTransactionsFragment.TAG)
            .addToBackStack(TotalTransactionsFragment.TAG)
            .commit();
      } else if (v.getId() == R.id.tax) {
        getSupportFragmentManager().beginTransaction()
            .add(android.R.id.content,
                UnitTrackingFragment.newInstance(getTitle(viewName), viewName),
                UnitTrackingFragment.TAG)
            .addToBackStack(generateFragmentName(viewName))
            .commit();
      } else {
        getSupportFragmentManager().beginTransaction()
            .add(android.R.id.content,
                TrackTransactionFragment.newInstance(getTitle(viewName), viewName),
                generateFragmentName(viewName))
            .addToBackStack(generateFragmentName(viewName))
            .commit();
      }
    }
  };

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    LinearLayout table = (LinearLayout) findViewById(R.id.root_view);
    assert table != null;

    for (int row = 0; row < table.getChildCount(); row++) {
      ViewGroup rowsGroup = ((ViewGroup) table.getChildAt(row));
      for (int column = 0; column < rowsGroup.getChildCount(); column++) {
        rowsGroup.getChildAt(column).setOnClickListener(onItemClickListener);
      }
    }
  }

  public static String generateFragmentName(String viewName) {
    return ITransactionFragment.TAG + "/" + viewName;
  }

  public String getTitle(String viewName) {
    return getResources().getString(
        getResources().getIdentifier(viewName + "_title", "string", getPackageName()));
  }
}
