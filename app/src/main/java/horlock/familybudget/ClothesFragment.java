package horlock.familybudget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import horlock.familybudget.models.Clothes;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ClothesFragment extends DialogFragment{

    Spinner spinner;
    Realm realm;
    Button doneButton;
    ArrayList<Clothes> clothes;
    TextView price, fullPrice;
    public int currentPosition = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.clothes_fragment, container, false);
        spinner = (Spinner) rootView.findViewById(R.id.clothes_month);
        price = (TextView)rootView.findViewById(R.id.clothes_price);
        fullPrice = (TextView)rootView.findViewById(R.id.clothes_year);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.month, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(adapter);

        final RealmConfiguration realmConfig = new RealmConfiguration.Builder(getContext()).build();
        realm = Realm.getInstance(realmConfig);
        RealmResults<Clothes> realmResults = realm.where(Clothes.class).findAll();

        clothes = new ArrayList<>();
        copyToClothesList(realmResults, clothes);
        updateTotalPrice();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView currentMonth = (TextView) view;
                boolean checker = false;
                if (clothes != null && !clothes.isEmpty()) {
                    for (int i = 0; i < clothes.size(); i++) {
                        if (clothes.get(i).getMonth().equals(currentMonth.getText())) {
                            checker = true;
                            currentPosition = i;
                            if (clothes.get(i).getMoney() != 0) {
                                price.setText(String.valueOf(clothes.get(i).getMoney()));
                            } else {
                                price.setText("");
                            }

                        }
                    }
                }
                if (!checker) {
                    price.setText("");
                    Clothes newClothes = new Clothes();
                    newClothes.setMonth(String.valueOf(currentMonth.getText()));
                    clothes.add(newClothes);
                    currentPosition = clothes.size() - 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                price.setText("");
            }
        });

        doneButton = (Button)rootView.findViewById(R.id.clothes_done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                realm.clear(Clothes.class);
                for (int i = 0; i < clothes.size(); i++) {
                    realm.copyToRealmOrUpdate(clothes.get(i));
                }
                realm.commitTransaction();
                dismiss();
            }
        });

        price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!TextUtils.isEmpty(s)){
                    clothes.get(currentPosition).setMoney(Integer.parseInt(s.toString()));
                    updateTotalPrice();
                }
            }
        });

        return rootView;
    }

    private void copyToClothesList(RealmResults<Clothes> realmResults, ArrayList<Clothes> clothes) {
        if(realmResults != null && !realmResults.isEmpty()){
            for (int i = 0; i < realmResults.size(); i++) {
                Clothes clo = new Clothes();
                clo.setMonth(realmResults.get(i).getMonth());
                clo.setMoney(realmResults.get(i).getMoney());
                clothes.add(i, clo);
            }
        }
    }

    public void updateTotalPrice() {
        int totalPrice = 0;
        if (clothes != null && !clothes.isEmpty()) {
            for (int i = 0; i < clothes.size(); i++) {
                totalPrice += clothes.get(i).getMoney();
            }
            fullPrice.setText(String.valueOf(totalPrice));
        }
    }

}
