package horlock.familybudget.utils;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

public class BindingTools {

  @BindingAdapter("bind:dependVisibilityOnStr")
  public static void viewVisibilityDependsOnStr(View view, CharSequence data){
    view.setVisibility(TextUtils.isEmpty(data) ? View.GONE : View.VISIBLE);
  }

  @BindingAdapter("bind:dependVisibilityOnStrInverse")
  public static void viewVisibilityDependsOnStrInverse(View view, CharSequence data){
    view.setVisibility(TextUtils.isEmpty(data) ? View.VISIBLE : View.GONE);
  }

  @BindingAdapter("bind:price")
  public static void setPrice(TextView editText, double price){
    if(price == 0){
      editText.setText(null);
    }else{
      editText.setText(String.valueOf(price));
    }
  }

}
