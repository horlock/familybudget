package horlock.familybudget;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import horlock.familybudget.models.Children;

public class ChildrenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int DATA_TYPE = 0;
    private static final int TEXT_TYPE = 1;
    private static final int FULL_DATA_TYPE = 2;
    ArrayList<Children> childrenArrayList;

    private void updateTotalMoneySpent() {
        int totalMoney = 0;
        if(childrenArrayList != null && !childrenArrayList.isEmpty()){
            for (int i = 0; i < childrenArrayList.size(); i++) {
                totalMoney += childrenArrayList.get(i).getMoney();
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            switch (viewType) {
                case TEXT_TYPE :
                    View viewText = inflater.inflate(R.layout.year_line, parent, false);
                    viewHolder = new TextViewHolder(viewText);
                    break;
                case DATA_TYPE :
                    View dataView = inflater.inflate(R.layout.child_line, parent, false);
                    viewHolder = new DataViewHolder(dataView);
                    break;
                case FULL_DATA_TYPE:
                    View fullDataView = inflater.inflate(R.layout.total_child_line, parent, false);
                    viewHolder = new FullDataViewHolder(fullDataView);
                    break;
            }
            return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case DATA_TYPE :
                DataViewHolder viewHolder = (DataViewHolder) holder;
                viewHolder.setInfo(childrenArrayList.get(position));
                break;
            case FULL_DATA_TYPE:
                FullDataViewHolder viewHolder1 = (FullDataViewHolder) holder;
                viewHolder1.setChildren(childrenArrayList.get(position - childrenArrayList.size() - 1));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return childrenArrayList.size()*2 + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position < childrenArrayList.size()){
            return DATA_TYPE;
        }else if(position == childrenArrayList.size()){
            return TEXT_TYPE;
        }else{
            return FULL_DATA_TYPE;
        }
    }

    public void addChild(Children child) {
        childrenArrayList.add(child);
        notifyItemInserted(childrenArrayList.size());
    }

    public void setList(ArrayList<Children> list) {
        this.childrenArrayList = list;
        updateTotalMoneySpent();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        int newMonth;
        Children newChild;
        TextView name, money;

        public DataViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.child_name);
            money = (TextView) itemView.findViewById(R.id.child_money);
        }

        public void setInfo(Children child){
            newChild = child;

            name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!TextUtils.isEmpty(s)){
                        newChild.setName(s.toString());
                        updateTotalMoneySpent();
                    }
                }
            });
            money.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!TextUtils.isEmpty(s)){
                        newChild.setMonth(Integer.parseInt(s.toString()));
                        updateTotalMoneySpent();
                    }
                }
            });
        }
    }

    class TextViewHolder extends RecyclerView.ViewHolder{

        public TextViewHolder(View itemView) {
            super(itemView);
        }


    }

    class FullDataViewHolder extends RecyclerView.ViewHolder{

        Children newChild;
        TextView name, money;

        public FullDataViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.total_child_name);
            money = (TextView) itemView.findViewById(R.id.total_child_money);
        }

        public void setChildren(Children child){
            newChild = child;

            name.setText(newChild.getName());
            money.setText(String.valueOf(newChild.getMoney()));
        }
    }
}
