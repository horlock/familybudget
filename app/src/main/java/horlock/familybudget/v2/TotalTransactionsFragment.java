package horlock.familybudget.v2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import horlock.familybudget.R;
import horlock.familybudget.models.ITransactionComposition;
import horlock.familybudget.models.Realms;
import horlock.familybudget.models.Transaction;
import io.realm.Realm;
import java.util.Arrays;

import static horlock.familybudget.v2.TransactionsAdapter.KEY_MONTH;
import static horlock.familybudget.v2.TransactionsAdapter.LOCAL_MONTH_CHANGED;

public class TotalTransactionsFragment extends Fragment {

  public static final String TAG = "fragment:total-transactions-info";

  private TransactionsAdapter<Transaction> adapter;

  private BroadcastReceiver monthChangeReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      if (LOCAL_MONTH_CHANGED.equals(intent.getAction())) {
        loadMonthly(intent.getIntExtra(KEY_MONTH, 0));
      }
    }
  };

  protected void loadMonthly(int currentMonth) {
    Realm realm = Realm.getDefaultInstance();
    Realms.MoneySpendingInfo.Builder builder = new Realms.MoneySpendingInfo.Builder(currentMonth);
    for (Realms.TransactionsChain chain : Realms.CHAINS) {
      chain.proceed(realm, builder);
    }
    Realms.MoneySpendingInfo info = builder.build();
    Transaction outcome = new Transaction() {
      @Override public String getName() {
        return "Outcome";
      }
    };
    outcome.setTransferred(info.outcome());

    Transaction income = new Transaction() {
      @Override public String getName() {
        return "Income";
      }
    };
    income.setTransferred(info.income());

    if (currentMonth == -1) {
      adapter.setMonthly(Arrays.asList(outcome, income));
    } else {
      adapter.setYearly(Arrays.asList(outcome, income));
    }
  }

  @Override public void onResume() {
    super.onResume();
    LocalBroadcastManager.getInstance(getContext())
        .registerReceiver(monthChangeReceiver, new IntentFilter(LOCAL_MONTH_CHANGED));
  }

  @Override public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(monthChangeReceiver);
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.content);
    recyclerView.setAdapter(adapter = new TransactionsAdapter<Transaction>() {
      @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TransactionsAdapter.INCOME_VIEW) {
          return new TotalPriceHolder(parent);
        }
        return super.onCreateViewHolder(parent, viewType);
      }
    });
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
    toolbar.setTitle("Бюджет");

    loadMonthly(-1);
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.track_incomes_fragment, container, false);
  }

  static class TotalPriceHolder extends TransactionsAdapter.TransactionViewHolder<Transaction> {

    public TotalPriceHolder(ViewGroup parent) {
      super(LayoutInflater.from(parent.getContext())
          .inflate(R.layout.budget_item_layout, parent, false));
    }

    @Override
    public TransactionsAdapter.TransactionViewHolder<Transaction> setTransaction(Transaction object,
        ITransactionComposition<Transaction> composition) {
      this.composition = composition;
      this.transaction = object;

      name.setText(object.getName());
      priceInput.setText(String.valueOf(object.getTransferred()));

      return this;
    }
  }
}
