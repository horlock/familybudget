package horlock.familybudget.v2;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import horlock.familybudget.R;
import horlock.familybudget.models.ITransactionComposition;
import horlock.familybudget.utils.BindingTools;
import io.realm.Realm;
import io.realm.RealmObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.support.v7.widget.RecyclerView.ViewHolder;

/**
 * Адаптер - это связь между Списком и Данными (здесь мы описываем как эти данные
 * будут отображаться в списке)
 */
public class TransactionsAdapter<ITransaction extends RealmObject>
    extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private static final Calendar calendarInstance = Calendar.getInstance();
  public final static String LOCAL_MONTH_CHANGED = "familybudget.income.month_changed";
  public final static String KEY_MONTH = "arg:month";

  public static final int HEADER_VIEW = 0;
  public static final int INCOME_VIEW = 1;

  // список для месяца
  protected List<ITransaction> monthly = new ArrayList<>();
  // список для года
  protected List<ITransaction> yearly = new ArrayList<>();

  protected ITransactionComposition<ITransaction> composition;

  /**
   * Описываем какой класс отвечает за тип данных, те у нас может быть заголовок
   * или транзакция
   */
  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    final LayoutInflater factory = LayoutInflater.from(parent.getContext());
    if (viewType == HEADER_VIEW) {
      return new HeaderViewHolder(factory, parent);
    } else {
      return TransactionViewHolder.<ITransaction>newInstance(factory, parent).setAdapter(this);
    }
  }

  /**
   * Добавляем транзакцию в наш список
   */
  public void addTransaction(ITransaction transaction) {
    monthly.add(transaction);
    yearly.add(transaction);

    final int monthlyPos = monthly.size();
    final int yearlyPos = monthlyPos + yearly.size();

    notifyItemInserted(monthlyPos);
    notifyItemInserted(yearlyPos);
  }

  /**
   * Обновляем View на основе переданной транзацкии
   */
  protected void reBindSynchronizedItem(ITransaction transaction) {
    int mIndex = 1 + monthly.indexOf(transaction);
    int yIndex = 2 + monthly.size() + yearly.indexOf(transaction);

    notifyItemChanged(mIndex);
    notifyItemChanged(yIndex);
  }

  /**
   * Удаляем транзакцию на позиции
   */
  public void remove(ITransaction transaction) {
    int mPos = remove(monthly, transaction);
    int yPos = remove(yearly, transaction);

    if (mPos >= 0) {
      notifyItemRemoved(mPos);
    }

    if (yPos >= 0) {
      int startIndex = 1 + monthly.size();
      notifyItemRemoved(startIndex + yPos);
    }
  }

  public static <T> int remove(List<T> items, T item) {
    int index = items.indexOf(item);
    if (index >= 0) { // items exists
      items.remove(index);
    }
    return index;
  }

  /**
   * Composition Object -> View
   */
  public void setComposition(ITransactionComposition<ITransaction> composition) {
    this.composition = composition;
  }

  public ITransactionComposition<ITransaction> getComposition() {
    return composition;
  }

  /**
   * Передаем месячные данные для отображения в списке
   */
  public void setMonthly(List<ITransaction> monthlyItems) {
    if (!monthly.isEmpty()) {
      int removedItemsCount = monthly.size();
      monthly.clear();
      notifyItemRangeRemoved(1, removedItemsCount);
    }

    monthly.addAll(monthlyItems);
    notifyItemRangeInserted(1, monthlyItems.size());
  }

  public List<ITransaction> getMonthly() {
    return monthly;
  }

  /**
   * Передаем данные за год для отображения в списке
   */
  public void setYearly(List<ITransaction> yearlyItems) {
    int startIndex = 2 + monthly.size();

    if (!yearly.isEmpty()) {
      int removedItemsCount = yearly.size();
      yearly.clear();
      notifyItemRangeRemoved(startIndex, removedItemsCount);
    }

    yearly.addAll(yearlyItems);
    notifyItemRangeInserted(startIndex, yearlyItems.size());
  }

  public List<ITransaction> getYearly() {
    return yearly;
  }

  /**
   * Берем элемент на позиции те если это начальные элемента списка
   * соответвественно мы берем из месячного списка, если мы перешли порог
   * месячных элементов то берем элементы из годовалых элементов
   */
  protected ITransaction getProperItem(int position) {
    if (--position < monthly.size()) {
      return monthly.get(position);
    }

    position -= monthly.size();
    position -= 1;

    if (position < yearly.size()) {
      return yearly.get(position);
    }

    return null;
  }

  /**
   * Смотрим если это заголок то даем ему соответствующий текст
   * если это транзакция то задаем ему элемент
   */
  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    if (isHeader(position)) {
      final Context context = holder.itemView.getContext();
      CharSequence title;
      boolean canFilter;
      if (position == 0) { //monthly header
        canFilter = true;
        title = context.getString(R.string.adapter_filter_by_month);
      } else {
        canFilter = false;
        title = context.getString(R.string.adapter_filter_by_year);
      }
      ((HeaderViewHolder) holder).bind(title, canFilter);
    } else {
      //noinspection unchecked
      TransactionViewHolder<ITransaction> vh = (TransactionViewHolder<ITransaction>) holder;
      vh.markAsYearly(isYearlyItem(position)).setTransaction(getProperItem(position), composition);
    }
  }

  /**
   * Проверяем если позиция для годовалого объекта
   */
  public boolean isYearlyItem(int position) {
    return position - 1 > monthly.size() && position - monthly.size() - 2 < yearly.size();
  }

  /**
   * Проверяем элемент на позиции заголовок или транзакция
   */
  public boolean isHeader(int position) {
    if (position == 0) {
      return true;
    } else if (position == 1 + monthly.size()) {
      return true;
    }
    return false;
  }

  /**
   * Указываем где какие элементы расположены
   */
  @Override public int getItemViewType(int position) {
    if (isHeader(position)) {
      return HEADER_VIEW;
    } else {
      return INCOME_VIEW;
    }
  }

  /**
   * Возвращаем колличество элементов на экране
   */
  @Override public int getItemCount() {
    int itemsCount = 2;
    if (!monthly.isEmpty()) {
      itemsCount += monthly.size();
    }
    if (!yearly.isEmpty()) {
      itemsCount += yearly.size();
    }
    return itemsCount;
  }

  public static class TransactionViewHolder<ITransaction extends RealmObject> extends ViewHolder
      implements TextView.OnEditorActionListener {
    public static <ITransaction extends RealmObject> TransactionViewHolder<ITransaction> newInstance(
        LayoutInflater factory, ViewGroup parent) {

      return new TransactionViewHolder<>(
          factory.inflate(R.layout.income_item_layout, parent, false));
    }

    protected TransactionsAdapter<ITransaction> adapter;
    protected ITransaction transaction;
    protected ITransactionComposition<ITransaction> composition;

    protected boolean isYearlyItem;

    protected EditText nameInput;
    protected TextView priceInput;
    protected TextView name;

    public TransactionViewHolder(View itemView) {
      super(itemView);
      this.name = (TextView) itemView.findViewById(R.id.name_view);
      this.nameInput = (EditText) itemView.findViewById(R.id.name_input_field);
      this.priceInput = (TextView) itemView.findViewById(R.id.price_input_field);

      if(nameInput != null && priceInput != null) {
        this.nameInput.setOnEditorActionListener(this);
        this.priceInput.setOnEditorActionListener(this);
      }
    }

    /** Отмечаем элемент как годовалый */
    protected TransactionViewHolder<ITransaction> markAsYearly(boolean isYearlyItem) {
      this.isYearlyItem = isYearlyItem;
      return this;
    }

    public TransactionViewHolder<ITransaction> setAdapter(
        TransactionsAdapter<ITransaction> adapter) {
      this.adapter = adapter;
      return this;
    }

    /** задаем транзакцию и здесь описываем как его отображать
     * куда какие данные указывать и в каком месте их отображать  */
    public TransactionViewHolder<ITransaction> setTransaction(ITransaction object,
        ITransactionComposition<ITransaction> composition) {
      this.transaction = object;
      this.composition = composition;
      this.priceInput.setEnabled(!isYearlyItem);

      CharSequence name = composition.getName(object);

      BindingTools.setPrice(this.priceInput, composition.getTransferred(object));
      BindingTools.viewVisibilityDependsOnStr(this.name, name);
      BindingTools.viewVisibilityDependsOnStrInverse(this.nameInput, name);

      this.name.setText(name);
      this.nameInput.setText(name);

      if (isYearlyItem) {
        this.nameInput.setVisibility(View.GONE);
        this.name.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(name)) {
          this.name.setText(R.string.pending_name_typing);
        }
      }
      return this;
    }

    public Object getItem() {
      return transaction;
    }

    /**
     * По нажатию на клавиатуре кнопки Done мы сохраняем данные и обнолвяем этот объект
     * на годовалой позиции
     */
    @Override public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
      if (actionId == EditorInfo.IME_ACTION_DONE) {
        final Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        if (!TextUtils.isEmpty(nameInput.getText())) {
          composition.setName(transaction, nameInput.getText());
        }
        if (v == priceInput && !TextUtils.isEmpty(priceInput.getText())) {
          composition.setTransferred(transaction, Integer.valueOf(priceInput.getText().toString()));
        }
        realm.commitTransaction();
        notifySyncedItemChanged();
        return true;
      }
      return false;
    }

    protected void notifySyncedItemChanged() {
      adapter.reBindSynchronizedItem(transaction);
    }
  }

  /** Класс для отображения заголовков */
  public static class HeaderViewHolder extends ViewHolder {

    private Spinner spinner;
    private TextView title;

    public HeaderViewHolder(LayoutInflater factory, ViewGroup parent) {
      super(factory.inflate(R.layout.filter_item_layout, parent, false));
      title = (TextView) itemView.findViewById(R.id.title);

      spinner = (Spinner) itemView.findViewById(R.id.month_filter);
      spinner.setSelection(calendarInstance.get(Calendar.MONTH));
      spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
          LocalBroadcastManager.getInstance(view.getContext())
              .sendBroadcast(new Intent(LOCAL_MONTH_CHANGED).putExtra(KEY_MONTH, position));
        }

        @Override public void onNothingSelected(AdapterView<?> parent) {

        }
      });
    }

    public void bind(CharSequence title, boolean canFilter) {
      this.title.setText(title);
      this.spinner.setVisibility(canFilter ? View.VISIBLE : View.GONE);
    }
  }
}