package horlock.familybudget.v2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import horlock.familybudget.R;
import horlock.familybudget.models.ITransactionComposition;
import horlock.familybudget.models.Realms;
import horlock.familybudget.models.UnitTransaction;
import horlock.familybudget.utils.BindingTools;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import java.util.Collections;

/**
 * Класс для слежение за тразакциями по газам электричестве на основе ITransactionFragment
 */
public class UnitTrackingFragment extends ITransactionFragment<UnitTransaction> {
  public static final String TAG = "fragment:electricity-tracking";

  private SharedPreferences preferences;

  public static UnitTrackingFragment newInstance(String title, String type) {
    Bundle args = new Bundle();
    args.putString(KEY_TITLE, title);
    args.putString(KEY_TYPE, type);

    UnitTrackingFragment fragment = new UnitTrackingFragment();
    fragment.setArguments(args);
    return fragment;
  }

  /**
   * Их нельзя swipe gesture делать
   */
  @Override
  protected int getMovementFlags(ItemTouchHelper.Callback callback, RecyclerView recyclerView,
      RecyclerView.ViewHolder viewHolder) {
    if (viewHolder instanceof ElectricityHolder && ((ElectricityHolder) viewHolder).isYearlyItem) {
      return 0;
    }
    return super.getMovementFlags(callback, recyclerView, viewHolder);
  }

  @Override protected void onMonthChanged(Intent intent) {
    super.onMonthChanged(intent);

    ensureMonthHasOneItem();
  }

  /**
   * Метод проверяет если у месяца нет видимого объекта транзакции
   * то добавляем его
   */
  protected void ensureMonthHasOneItem() {
    if (adapter.getMonthly().isEmpty()) {
      Realm realm = Realm.getDefaultInstance();
      realm.beginTransaction();
      UnitTransaction unitTransaction = realm.createObject(UnitTransaction.class);
      unitTransaction.setMonth(currentMonth);
      unitTransaction.setType(getArguments().getString(KEY_TYPE));
      realm.commitTransaction();

      adapter.addTransaction(unitTransaction);
    }
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    preferences = ((UnitTransaction.UnitTransactionComposition) composition).getPreferences();

    loadYearlyTransaction();

    final View globalPriceLayout = LayoutInflater.from(view.getContext())
        .inflate(R.layout.include_set_price_layout, (ViewGroup) view, false);

    EditText globalPriceView = (EditText) globalPriceLayout.findViewById(R.id.global_price);
    globalPriceView.setText(String.valueOf(
        preferences.getFloat(UnitTransaction.UnitTransactionComposition.KEY_ELECTRICITY_PRICE, 0)));
    globalPriceView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          /**
           * По нажатию ок кнопки  сохраняем цену на элекртричество или  газ
           */
          if (!TextUtils.isEmpty(v.getText())) {
            float price = Float.valueOf(v.getText().toString());
            preferences.edit()
                .putFloat(UnitTransaction.UnitTransactionComposition.KEY_ELECTRICITY_PRICE, price)
                .apply();
            adapter.notifyItemRangeChanged(0, adapter.getItemCount());
            return true;
          }
        }
        return false;
      }
    });

    ((ViewGroup) view).addView(globalPriceLayout, 1,
        new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT));

    Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
    toolbar.getMenu().removeItem(R.id.create_new_transaction);
  }

  /**
   * Загружаем данные на год и создаем один объект который суммирует все данные и отображает
   */
  protected void loadYearlyTransaction() {
    RealmResults<UnitTransaction> results = query().findAllSorted("month", Sort.ASCENDING);
    if (!results.isEmpty()) {
      UnitTransaction unitTransaction = new UnitTransaction();
      unitTransaction.setTransferred(results.sum("transferred").doubleValue());
      unitTransaction.setStartValue(results.first().getStartValue());
      unitTransaction.setEndValue(results.last().getEndValue());
      unitTransaction.setMonth(-1);

      adapter.setYearly(Collections.singletonList(unitTransaction));
    }
  }

  @Override public TransactionsAdapter<UnitTransaction> getAdapter() {
    if (adapter == null) {
      adapter = new ElectricityAdapter();
    }
    return adapter;
  }

  /**
   * Стандартный Адаптер добавляет сразу в год и месяц, тут мы переписали
   * что бы он добавлял и удалял только в месяц
   */
  public class ElectricityAdapter extends TransactionsAdapter<UnitTransaction> {

    @Override public void addTransaction(UnitTransaction unitTransaction) {
      monthly.add(unitTransaction);

      final int monthlyPos = monthly.size();
      notifyItemInserted(monthlyPos);
    }

    @Override public void remove(UnitTransaction unitTransaction) {
      int mPos = remove(monthly, unitTransaction);

      if (mPos >= 0) {
        notifyItemRemoved(mPos);
        loadYearlyTransaction();
      }
    }

    @Override protected void reBindSynchronizedItem(UnitTransaction unitTransaction) {
      loadYearlyTransaction();

      notifyItemChanged(1 + monthly.indexOf(unitTransaction));
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      if (viewType == TransactionsAdapter.INCOME_VIEW) {
        return new ElectricityHolder(LayoutInflater.from(parent.getContext())
            .inflate(R.layout.electricity_item_layout, parent, false)).setAdapter(this);
      }
      return super.onCreateViewHolder(parent, viewType);
    }
  }

  /**
   * Переписываем вид отображения
   */
  public static class ElectricityHolder
      extends TransactionsAdapter.TransactionViewHolder<UnitTransaction> {

    EditText startValue;
    EditText endValue;

    TextView deltaValue;

    SharedPreferences preferences;

    public ElectricityHolder(View itemView) {
      super(itemView);

      startValue = (EditText) itemView.findViewById(R.id.register_start_value);
      endValue = (EditText) itemView.findViewById(R.id.register_end_value);
      deltaValue = (TextView) itemView.findViewById(R.id.register_delta_value);

      startValue.setOnEditorActionListener(this);
      endValue.setOnEditorActionListener(this);
    }

    /**
     * Вводимые данные только у месяца
     * и отображается больше данных нежели в обычном transaction
     */
    @Override public TransactionsAdapter.TransactionViewHolder<UnitTransaction> setTransaction(
        UnitTransaction object, ITransactionComposition<UnitTransaction> composition) {
      this.transaction = object;
      this.composition = composition;

      preferences = ((UnitTransaction.UnitTransactionComposition) composition).getPreferences();

      BindingTools.setPrice(this.priceInput, composition.getTransferred(object));

      ((ViewGroup) startValue.getParent().getParent()).setVisibility(
          isYearlyItem ? View.GONE : View.VISIBLE);
      ((ViewGroup) endValue.getParent().getParent()).setVisibility(
          isYearlyItem ? View.GONE : View.VISIBLE);

      if (!isYearlyItem) {
        startValue.setText(String.valueOf(object.getStartValue()));
        endValue.setText(String.valueOf(object.getEndValue()));
      }

      int delta = object.getEndValue() - object.getStartValue();
      if (delta > 0) {
        deltaValue.setText(String.valueOf(delta));
      } else {
        deltaValue.setText(null);
      }
      return this;
    }

    /**
     * Обновляем данные по нажатию по кнопке Done на клавиатуре
     * и обновляем базу данных
     */
    @Override public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
      UnitTransaction unitTransaction = (UnitTransaction) getItem();

      int startKv = TextUtils.isEmpty(startValue.getText()) ? 0
          : Integer.valueOf(startValue.getText().toString());

      int endKv = TextUtils.isEmpty(endValue.getText()) ? 0
          : Integer.valueOf(endValue.getText().toString());

      if (endKv - startKv >= 0) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        unitTransaction.setStartValue(startKv);
        unitTransaction.setEndValue(endKv);
        unitTransaction.setTransferred((endKv - startKv));

        realm.commitTransaction();
        notifySyncedItemChanged();
      }
      return true;
    }
  }

  @Override protected RealmQuery<UnitTransaction> query() {
    return super.query().equalTo("type", getArguments().getString(KEY_TYPE));
  }

  @Override protected Class<UnitTransaction> transactionClass() {
    return UnitTransaction.class;
  }

  @Override protected ITransactionComposition<UnitTransaction> getComposition() {
    //noinspection unchecked
    return Realms.REALMS_MAP.get(UnitTransaction.class);
  }
}
