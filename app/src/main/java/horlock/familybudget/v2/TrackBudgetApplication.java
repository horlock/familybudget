package horlock.familybudget.v2;

import android.app.Application;
import android.content.Context;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

public class TrackBudgetApplication extends Application{

  private static Context context;

  public static Context getContext() {
    return context;
  }

  @Override public void onCreate() {
    super.onCreate();
    context = this;

    Realm.setDefaultConfiguration(new RealmConfiguration.Builder(this).build());
    Timber.plant(new Timber.DebugTree());
  }
}
