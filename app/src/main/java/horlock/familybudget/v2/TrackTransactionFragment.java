package horlock.familybudget.v2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import horlock.familybudget.R;
import horlock.familybudget.models.ITransactionComposition;
import horlock.familybudget.models.Realms;
import horlock.familybudget.models.Transaction;
import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Класс для слежение за тразакциями на основе ITransactionFragment
 */
public class TrackTransactionFragment extends ITransactionFragment<Transaction>{
  public final static String TAG = "fragment:transaction-tracking";

  /**
   * Метод для создания данного фрагмента где есть объязательные параметры
   * это название фрагмента и название налога (для фильтра)
   */
  public static ITransactionFragment newInstance(String title, String filterBy) {
    Bundle bundle = new Bundle();
    bundle.putString(KEY_TYPE, filterBy);
    bundle.putString(KEY_TITLE, title);

    ITransactionFragment fragment = new TrackTransactionFragment();
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (!getArguments().containsKey(KEY_TYPE)) {
      throw new RuntimeException("\"type\" argument wasn't provided");
    }
  }

  /**
   * Добавляем новый элемент на экран и в базу данных
   * по нажатию на элемент меню
   */
  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if(item.getItemId() == R.id.create_new_transaction){
      final Realm realm = Realm.getDefaultInstance();
      realm.beginTransaction();
      Transaction transaction = Realm.getDefaultInstance()
          .createObject(Transaction.class);
      transaction.setMonth(currentMonth);
      transaction.setType(getArguments().getString(KEY_TYPE));
      adapter.addTransaction(transaction);
      realm.commitTransaction();
      return true;
    }
    return false;
  }

  @Override protected RealmQuery<Transaction> query() {
    return super.query().equalTo("type", getArguments().getString(KEY_TYPE));
  }

  @Override protected Class<Transaction> transactionClass() {
    return Transaction.class;
  }

  @Override protected ITransactionComposition<Transaction> getComposition() {
    //noinspection unchecked
    return Realms.REALMS_MAP.get(Transaction.class);
  }
}
