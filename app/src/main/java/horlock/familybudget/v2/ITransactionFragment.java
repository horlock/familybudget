package horlock.familybudget.v2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import horlock.familybudget.R;
import horlock.familybudget.models.ITransactionComposition;
import horlock.familybudget.v2.TransactionsAdapter.TransactionViewHolder;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

import static horlock.familybudget.v2.TransactionsAdapter.KEY_MONTH;
import static horlock.familybudget.v2.TransactionsAdapter.LOCAL_MONTH_CHANGED;

/**
 * Абстрактный фрагмент для отображения транзакций с базовыми функциями
 * swipe-to-delete, загрузка годовых и месячных типов
 *
 * @param <ITransaction> Абстрактный тип объекта, мы не указываем его явно, от каждого
 * Fragment зависит какой проект
 */
public class ITransactionFragment<ITransaction extends RealmObject> extends Fragment {
  public final static String TAG = "fragment:monthly-tracking";
  public static final String KEY_TITLE = "arg:title";
  public static final String KEY_TYPE = "arg:filterBy";

  protected int currentMonth;
  protected TransactionsAdapter<ITransaction> adapter;
  protected ITransactionComposition<ITransaction> composition;

  /** Это слушатель для изменения месяца который передается из Adapter'a */
  private BroadcastReceiver monthChangeReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      if (LOCAL_MONTH_CHANGED.equals(intent.getAction())) {
        onMonthChanged(intent);
      }
    }
  };

  /** интерфейс для swipe-to-dismiss */
  private ItemTouchHelper.Callback onDismissedBySwipe = new ItemTouchHelper.Callback() {
    @Override public int getMovementFlags(RecyclerView recyclerView, ViewHolder viewHolder) {
      return ITransactionFragment.this.getMovementFlags(this, recyclerView, viewHolder);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder, ViewHolder target) {
      return false;
    }

    /** Если View был убран с экрана удаляем его из Базы Данных и нашего экрана */
    @SuppressWarnings("unchecked") @Override public void onSwiped(ViewHolder viewHolder,
        int direction) {
      TransactionViewHolder<ITransaction> holder = (TransactionViewHolder<ITransaction>) viewHolder;
      ITransaction item = (ITransaction) holder.getItem();
      // удаляем из view
      adapter.remove(item);

      // удаляем из бд
      ///////////// REALM TRANSACTION /////////////
      Realm realm = Realm.getDefaultInstance();
      realm.beginTransaction();

      item.removeFromRealm();

      realm.commitTransaction();
      ///////////// END TRANSACTION /////////////

      assert getView() != null;
      Snackbar.make(getView(), R.string.transaction_deleted, Snackbar.LENGTH_SHORT).show();
    }
  };

  /** Мы указываем в какую сторону можно удалять с помощью жеста swipe */
  protected int getMovementFlags(ItemTouchHelper.Callback callback, RecyclerView recyclerView,
      ViewHolder viewHolder) {
    if (viewHolder instanceof TransactionViewHolder) { // только item'ы
      return ItemTouchHelper.Callback.makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE,
          ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
    }
    return 0;
  }

  /**
   * Вызывается только при изменение месяца пользователем,  соответствено и
   * обновляются данные на экране в соответствие
   */
  protected void onMonthChanged(Intent intent) {
    currentMonth = intent.getIntExtra(KEY_MONTH, 0);

    RealmResults<ITransaction> results = query().equalTo("month", currentMonth).findAll();

    adapter.setMonthly(results);
  }

  @Override public void onResume() {
    super.onResume();
    LocalBroadcastManager.getInstance(getContext())
        .registerReceiver(monthChangeReceiver, new IntentFilter(LOCAL_MONTH_CHANGED));
  }

  @Override public void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(monthChangeReceiver);
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.track_incomes_fragment, container, false);
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    /*
      Настраиваем View для отображения данных
      RecyclerView - список
      Toolbar - синяя навигационная панель на вверху экрана
      ItemTouchHelper - для swipe-to-dismiss feature
     */
    composition = getComposition();

    RecyclerView incomesListView = (RecyclerView) view.findViewById(R.id.content);
    incomesListView.setLayoutManager(new LinearLayoutManager(getContext()));
    incomesListView.setAdapter(getAdapter());

    adapter.setComposition(composition);
    adapter.setYearly(query().findAll());

    ItemTouchHelper helper = new ItemTouchHelper(onDismissedBySwipe);
    helper.attachToRecyclerView(incomesListView);

    Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
    toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
      @Override public boolean onMenuItemClick(MenuItem item) {
        return onOptionsItemSelected(item);
      }
    });
    toolbar.setTitle(getArguments().getString(KEY_TITLE));
    toolbar.getMenu()
        .add(0, R.id.create_new_transaction, 0, R.string.add_new_transaction)
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
  }

  /**
   * @return возвращает адаптер для списка (возможно custom adapter)
   */
  public TransactionsAdapter<ITransaction> getAdapter() {
    if (adapter == null) {
      adapter = new TransactionsAdapter<>();
    }
    return adapter;
  }

  /** Запрос на получение элементов из базы данных в зависимости от класса */
  protected RealmQuery<ITransaction> query() {
    return Realm.getDefaultInstance().where(transactionClass());
  }

  /** Абстрактный метод который возвращает какой класс(таблицу) в базе данных мы должны использовать */
  protected Class<ITransaction> transactionClass() {
    return null;
  }

  /** Composition между объектом таблицы и view  */
  protected ITransactionComposition<ITransaction> getComposition() {
    return null;
  }
}
