package horlock.familybudget;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import horlock.familybudget.models.Credit;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class CreditActivity extends AppCompatActivity {

    CreditAdapter creditAdapter;
    Realm realm;
    ImageButton addButton, doneButton;
    TextView monthPayment, yearPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit_activity);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.credit_recycler_view);
        final RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).build();
        realm = Realm.getInstance(realmConfig);
        RealmResults<Credit> realmResults = realm.where(Credit.class).findAll();
        ArrayList<Credit> creditArrayList = new ArrayList<>();
        copyDataFromRealm(realmResults, creditArrayList);
        creditAdapter = new CreditAdapter(creditArrayList);
        recyclerView.setAdapter(creditAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        addButton = (ImageButton) findViewById(R.id.add_credit_button);
        doneButton = (ImageButton) findViewById(R.id.credit_done);
        monthPayment = (TextView) findViewById(R.id.all_credit_month);
        yearPayment = (TextView) findViewById(R.id.all_credit_year);

        checkForNull(creditArrayList);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Credit credit = new Credit();
                creditAdapter.addCredit(credit);
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                realm.clear(Credit.class);
                for(int i = 0; i < creditAdapter.getCreditSize(); i++) {
                    if(creditAdapter.credits.get(i).getName() != null)
                        realm.copyToRealmOrUpdate(creditAdapter.credits.get(i));
                }
                realm.commitTransaction();
                finish();
            }
        });
    }

    private void checkForNull(ArrayList<Credit> creditArrayList) {
        if(creditArrayList != null && !creditArrayList.isEmpty()){
            updateTotalMonthPrice();
            updateTotalYearPrice();
        }
    }

    private void updateTotalYearPrice() {
        int totalYearPrice = 0;
        for(int i = 0; i < creditAdapter.getCreditSize(); i++){
            totalYearPrice += creditAdapter.credits.get(i).getYearPayment();
        }
        yearPayment.setText(String.valueOf(totalYearPrice));
    }

    private void updateTotalMonthPrice() {
        int totalMonthPrice = 0;
        for(int i = 0; i < creditAdapter.getCreditSize(); i++){
            totalMonthPrice += creditAdapter.credits.get(i).getMonthPayment();
        }
        monthPayment.setText(String.valueOf(totalMonthPrice));
    }


    private void copyDataFromRealm(RealmResults<Credit> realmResults, ArrayList<Credit> creditArrayList) {
        for (int i = 0; i < realmResults.size(); i++) {
            Credit credit = new Credit();
            credit.setName(realmResults.get(i).getName());
            credit.setYearPayment(realmResults.get(i).getYearPayment());
            credit.setMonthPayment(realmResults.get(i).getMonthPayment());
            creditArrayList.add(i, credit);
        }
    }
}
