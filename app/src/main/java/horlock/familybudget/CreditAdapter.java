package horlock.familybudget;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import horlock.familybudget.models.Credit;

public class CreditAdapter extends RecyclerView.Adapter<CreditAdapter.ViewHolder>{

    ArrayList<Credit> credits;

    public CreditAdapter(ArrayList<Credit> creditArrayList) {
        credits = creditArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.credit_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setCredit(credits.get(position));
    }

    @Override
    public int getItemCount() {
        return credits.size();
    }

    public void addCredit(Credit credit) {
        credits.add(credit);
        notifyItemInserted(credits.size());
    }

    public int getCreditSize() {
        return credits.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private Credit credit;
        TextView name, month, year;

        public ViewHolder(View v) {
            super(v);

            name = (TextView) v.findViewById(R.id.credit_name);
            month = (TextView) v.findViewById(R.id.month);
            year = (TextView) v.findViewById(R.id.year);
        }

        public void setCredit(Credit newCredit) {
            credit = newCredit;

            name.setText("");
            month.setText("");
            year.setText("");

            if(credit.getName() != null)
            name.setText(credit.getName());
            if(credit.getMonthPayment() != 0)
            month.setText(String.valueOf(credit.getMonthPayment()));
            if(credit.getYearPayment() != 0)
            year.setText(String.valueOf(credit.getYearPayment()));

            name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!TextUtils.isEmpty(s)) {
                        credit.setName(s.toString());
                    }
                }
            });
            month.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!TextUtils.isEmpty(s)) {
                        credit.setMonthPayment(Integer.parseInt(s.toString()));
                    }
                }
            });
            year.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!TextUtils.isEmpty(s)) {
                        credit.setYearPayment(Integer.parseInt(s.toString()));
                    }
                }
            });
        }
    }
}
