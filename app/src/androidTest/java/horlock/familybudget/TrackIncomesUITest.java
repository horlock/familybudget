package horlock.familybudget;

import android.support.test.espresso.action.ViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import horlock.familybudget.v2.ITransactionFragment;
import org.assertj.android.support.v4.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.assertj.android.recyclerview.v7.api.Assertions.assertThat;

@RunWith(AndroidJUnit4.class) @LargeTest public class TrackIncomesUITest {

  @Rule public ActivityTestRule<MainActivity> activityTestRule =
      new ActivityTestRule<>(MainActivity.class);

  @Test public void testOpenTrackingFragment() {
    onView(withText(R.string.income_journal_title)).perform(click());

    final MainActivity activity = activityTestRule.getActivity();

    Assertions.assertThat(activity.getSupportFragmentManager())
        .hasFragmentWithTag(ITransactionFragment.TAG);

    Assertions.assertThat(
        activity.getSupportFragmentManager().findFragmentByTag(ITransactionFragment.TAG))
        .isVisible();
  }

  @Test public void testAddingItem() {
    final MainActivity activity = activityTestRule.getActivity();
    ITransactionFragment fragment = (ITransactionFragment) activity.getSupportFragmentManager()
        .findFragmentByTag(ITransactionFragment.TAG);

    onView(withClassName(Matchers.containsString(Toolbar.class.getName())))
        .perform(ViewActions.pressMenuKey());

    onView(withText("Add")).perform(click());

    assert fragment.getView() != null;
    RecyclerView recyclerView = (RecyclerView) fragment.getView().findViewById(R.id.content);
    assertThat(recyclerView.getAdapter()).hasItemCount(2);
  }
}
